const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

function Books(books) {
    this.render = function (root) {
        const ul = document.createElement('ul');
        const array = books.forEach((elem) => {
            try {
                if (elem.author === undefined) {
                    throw new SyntaxError('author-undefined');
                }
                if (elem.name === undefined) {
                    throw new SyntaxError('name-undefined');
                }
                if (elem.price === undefined) {
                    throw new SyntaxError('price-undefined');
                }
                const li = document.createElement('li');
                li.textContent += elem.author + ' ' + ' ' + elem.name + ' ' + elem.price;
                ul.append(li);
                root.append(ul);
            } catch (e) {
                console.log(e.message);
            }
        })
    }
}
const book = new Books(books);
book.render(document.querySelector('#root'));